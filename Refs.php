<?php

namespace App\Dictionaries;

class Refs {

    const AUDIT_SUMMARY_BARS_OVERVIEW = "[
                        {id: 'name', label: 'Name'},
                        {id: 'income_ex_vat', label: 'Income Ex Vat', class:'ndc-col-right'},
                        {id: 'consumed_at_cost', label: 'Consumed At Cost', class:'ndc-col-right' },
                        {id: 'gp', label: 'GP (%)', class:'ndc-col-right' },
                        {id: 'estimated_gp', label: 'Estimated GP (%)', class:'ndc-col-right' },
                        {id: 'actual_gp', label: 'Actual GP (%)', class:'ndc-col-right' },
                        {id: 'surplus', label: 'Surplus (Deficit)', class:'ndc-col-right text-danger' },
                        {id: 'allowances', label: 'Allowances', class:'ndc-col-right' },
                    ]";

    const AUDIT_SUMMARY_LIQUOUR = "[
                        {id: 'name', label: 'Name'},
                        {id: 'last_closing_stock', label: 'Last C/S', class:'ndc-col-right'},
                        {id: 'opening_stock', label: 'O/S', class:'ndc-col-right' },
                        {id: 'purchases', label: 'Purch.', class:'ndc-col-right' },
                        {id: 'credits', label: 'Credits', class:'ndc-col-right' },
                        {id: 'transfers', label: 'Transf.', class:'ndc-col-right' },
                        {id: 'closing_stock', label: 'C/S', class:'ndc-col-right' },
                        {id: 'consumed_at_cost', label: 'Cons.Cost. (£)', class:'ndc-col-right' },
                        {id: 'consumed_retail', label: 'Cons.Retail (£)', class:'ndc-col-right' },
                        {id: 'sales_ratio', label: 'Sales Ratio (%)', class:'ndc-col-right' },
                        {id: 'gross_profit', label: 'Gross Profit (%)', class:'ndc-col-right' },
                        {id: 'days_stock', label: 'Days Stock', class:'ndc-col-right' },
                    ]";

    const AUDIT_SUMMARY_STOCK = "[
                {id: 'code', label: 'CODE', class:'ndc-col-right code-label'},
                {id: 'name', label: 'Name'},
                {id: 'size', label: 'Size'},
                {id: 'cost', label: 'Cost', class:'ndc-col-right'},
                {id: 'price', label: 'Price', class:'ndc-col-right'},
                {id: 'opening_stock', label: 'O/S', class:'ndc-col-right' },
                {id: 'credits', label: 'Credits', class:'ndc-col-right' },
                {id: 'purchases', label: 'Purch.', class:'ndc-col-right' },
                {id: 'transfers', label: 'Transf.', class:'ndc-col-right' },
                {id: 'closing_stock', label: 'C/S', class:'ndc-col-right' },
                {id: 'consumed', label: 'Sales', class:'ndc-col-right' }, 
                {id: 'breakage', label: 'Cons.Cost.', class:'ndc-col-right text-danger' },
                {id: 'ballance', label: 'Cons.Retail', class:'ndc-col-right text-danger' },
                {id: 'days_stock', label: 'Days Stock', class:'ndc-col-right' },
                {id: 'gross_profit', label: 'Gross Profit (%)', class:'ndc-col-right' },
                {id: 'stock_at_cost', label: 'Stock At Cost', class:'ndc-col-right' },
                {id: 'avg_daily_sales', label: 'AVG Daily Sales', class:'ndc-col-right' },
            ]";

    const AUDIT_SUMMARY_DELIVERY_NOTES = "[
                {id: 'name', label: 'Supplier'},
                {id: 'number', label: 'Delivery #', class:'ndc-col-right'},
                {id: 'order_number', label: 'Order #', class:'ndc-col-right' },
                {id: 'date', label: 'Date', class:'ndc-col-right' },
                {id: 'ref_number', label: 'Ref #', class:'ndc-col-right' },
                {id: 'value', label: 'Value (£)', class:'ndc-col-right' },
            ]";

    const AUDIT_SUMMARY_BARS = "[
                            {id: 'name', label: 'Name'},
                            {id: 'last_closing_stock', label: 'Last C/S', class:'ndc-col-right'},
                            {id: 'opening_stock', label: 'O/S', class:'ndc-col-right' },
                            {id: 'purchases', label: 'Purch.', class:'ndc-col-right' },
                            {id: 'credits', label: 'Credits', class:'ndc-col-right' },
                            {id: 'transfers', label: 'Transf.', class:'ndc-col-right' },
                            {id: 'closing_stock', label: 'C/S', class:'ndc-col-right' },
                            {id: 'consumed_at_cost', label: 'Cons.Cost. (£)', class:'ndc-col-right' },
                            {id: 'consumed_retail', label: 'Cons.Retail (£)', class:'ndc-col-right' },
                            {id: 'sales_ratio', label: 'Sales Ratio (%)', class:'ndc-col-right' },
                            {id: 'gross_profit', label: 'Gross Profit (%)', class:'ndc-col-right' },
                            {id: 'days_stock', label: 'Days Stock', class:'ndc-col-right' },
                        ]";

    const AUDIT_SUMMARY_DELIVERY_DETAILS = "[
                {id: 'name', label: 'Name'},
                {id: 'size', label: 'Size', class:'ndc-col-right'},
                {id: 'quantity', label: 'Quantity', class:'ndc-col-right' },
                {id: 'cost', label: 'Cost', class:'ndc-col-right' },
                {id: 'value', label: 'Value', class:'ndc-col-right' }
            ]";
}
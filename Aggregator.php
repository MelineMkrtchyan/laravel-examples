<?php

namespace App\Managers;

use App\Entities\Area;
use App\Entities\Audit;
use App\Entities\Delivery;
use App\Entities\Praudit;
use App\Entities\ProductType;
use App\Entities\Receipt;
use Facades\App\Managers\Calculators\CreditsAtCost;
use Facades\App\Managers\Calculators\PurchasesAtCost;
use Facades\App\Managers\Calculators\TransfersAtCost;
use App\Managers\Reports\ReportCollection;
use App\Managers\Reports\ReportRow;
use Facades\App\Managers\Calculators\Consumed;
use Facades\App\Managers\Calculators\IncomeExVat;
use Facades\App\Managers\Calculators\ConsumedAtCost;
use Facades\App\Managers\Calculators\ConsumedAtRetail;
use Facades\App\Managers\Calculators\GrossProfit;
use Facades\App\Managers\Calculators\DaysInStock;
use Facades\App\Managers\Calculators\SalesRatio;
use Facades\App\Managers\Calculators\Overage;
use Facades\App\Managers\Calculators\Shortage;
use Facades\App\Managers\Calculators\Balance;
use Facades\App\Managers\Calculators\ClosingStockAtCost;
use Facades\App\Managers\Stock;
use Facades\App\Managers\Calculators\StockAtCost;
use Illuminate\Support\Facades\Log;

class Aggregator extends Manager
{
    private $stock;

    private $vat = 1.2;

    public function __construct(Stock $stock)
    {
        $this->stock = $stock;
    }

    /**
     *
     * Returns all the delivery details
     *
     * @param array $options
     * @return object
     */
    public function aggregateDeliveryDetails($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $delivery_id = $this->getOptions('delivery_id', $options);

        $fixed = ['code', 'name', 'size', 'quantity', 'cost', 'value'];
        $accumulable = [];
        $items = new ReportCollection;

        $deliveries_builder = Delivery::with('supplier', 'preliveries' ,'preliveries.product')
            ->orderBy('id', 'desc');
        $this->addToBuilder($deliveries_builder, 'branch_id', $branch_id);
        $this->addToBuilder($deliveries_builder, 'audit_id', $audit_id);
        $this->addToBuilder($deliveries_builder, 'id', $delivery_id);
        $deliveries = $deliveries_builder->get();

        $deliveries->each(function($delivery) use ($branch_id, $audit_id, $fixed, $accumulable, &$items){

            if($delivery->hasPreliveries())
            {
                $delivery->preliveries->each(function($prelivery) use ($branch_id, $audit_id, &$delivery, &$items, $fixed, $accumulable) {

                    $row = new ReportRow($fixed, $accumulable);
                    $row->set('code', $prelivery->product->code);
                    $row->set('name', $prelivery->product->description);
                    $row->set('size', $prelivery->product->size->short_description);
                    $row->set('quantity', ffn($prelivery->quantity));
                    $row->set('cost', ffn($prelivery->cost));
                    $row->set('value', ffn((float)$prelivery->quantity*(float)$prelivery->cost));
                    $items->push($row);

                });
            }
        });
        return $items->aggregated(['value']);
    }


    /**
     *
     * Returns all the delivery details
     *
     * @param array $options
     * @return object
     */
    public function aggregateDeliveriesDetails($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);

        $fixed = ['code', 'name', 'size', 'quantity', 'cost', 'value'];
        $accumulable = [];
        $items = new ReportCollection;

        $deliveries_builder = Delivery::with('supplier', 'preliveries' ,'preliveries.product')
            ->orderBy('id', 'desc');
        $this->addToBuilder($deliveries_builder, 'branch_id', $branch_id);
        $this->addToBuilder($deliveries_builder, 'audit_id', $audit_id);
        $deliveries = $deliveries_builder->get();

        $deliveries->each(function($delivery) use ($branch_id, $audit_id, $fixed, $accumulable, &$items){

            if($delivery->hasPreliveries())
            {

                $hrow = new ReportRow(['name', 'number', 'order_number', 'date', 'ref_number'], [], ['is_header' => true]);
                $hrow->set('name', $delivery->supplier->name);
                $hrow->set('number', $delivery->code);
                $hrow->set('order_number', $delivery->order);
                $hrow->set('date', $delivery->date_formatted);
                $hrow->set('ref_number', $delivery->invoice);
                $items->push($hrow);

                $delivery->preliveries->each(function($prelivery) use ($branch_id, $audit_id, &$delivery, &$items, $fixed, $accumulable) {

                    $row = new ReportRow($fixed, $accumulable);
                    $row->set('code', $prelivery->product->code);
                    $row->set('name', $prelivery->product->description);
                    $row->set('size', $prelivery->product->size->short_description);
                    $row->set('quantity', ffn($prelivery->quantity));
                    $row->set('cost', ffn($prelivery->cost));
                    $row->set('value', ffn((float)$prelivery->quantity*(float)$prelivery->cost));
                    $items->push($row);

                });
            }



        });

        return $items->raw();
    }

    /**
     *
     * Returns aggregated columns for stock with subtotals
     * for each product type
     *
     * @param array $options
     * @return object
     */
    public function aggregateStock($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $items = new ReportCollection;
        $fixed = ['name', 'size'];
        $accumulables = [
            'code',
            'cost',
            'price',
            'opening_stock',
            'purchases',
            'credits',
            'closing_stock',
            'consumed',
            'transfers',
            'breakage',
            'ballance',
            'days_stock',
            'gross_profit',
            'stock_at_cost',
            'avg_daily_sales'
        ];

        $product_types = ProductType::with('products')->orderBy('name')->get();

        $rowGrossProfit = new ReportRow($fixed, $accumulables);
        $rowGrossProfit->set('name', 'Gross Profit');

        foreach ($product_types as $product_type) {
            /**
             * Getting the product ids that belong to this product type
             */
            $products_ids_product_type = $product_type->hasProducts() ? $product_type->products->pluck('id')->all() : [];

            /**
             * Now I get all the praudits that belong to this product type
             */
            $praudits_builder = Praudit::with('product', 'audit');
            $this->addToBuilder($praudits_builder, 'product_id', $products_ids_product_type);
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area_id);
            $praudits = $praudits_builder->get();

            if (count($praudits)) {


                foreach ($praudits as $praudit) {
                    $row = new ReportRow($fixed, $accumulables, ['group' => 'pt' . $product_type->id]);
                    $row->set('name', $praudit->product->description);
                    $row->set('size', $praudit->product->size->short_description);

                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);

                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);

                    $praudit_consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);

                    $consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);

                    $stock_at_cost = StockAtCost::calculate([
                        'closing' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);

                    $days_stock = DaysInStock::calculate([
                        'consumed_at_cost' => $consumed_at_cost,
                        'days_in_period' => $praudit->audit->total_days,
                        'closing_stock' => $stock_at_cost,
                    ]);

                    $praudit_closing_stock_at_cost = ClosingStockAtCost::calculate([
                        'closing_stock' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);

                    $price = Stock::getSellPrice([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                    ]);

                    $gp = GrossProfit::newCalculate([
                        'units' => $praudit->product->size->units_of_measure,
                        'price' => $price,
                        'vat' => $this->vat,
                        'cost' => $cost,
                    ]);

                    $price = Stock::getSellPrice([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                    ]);

//                    $praudit_consumed_at_retail = Stock::getTillCheckConsumption([
//                        'audit_id' => $audit_id,
//                        'area_id' => $praudit->area_id,
//                        'product_id' => $praudit->product_id,
//                        'sell_price' => $price,
//                    ]);

                    $praudit_consumed_at_retail = $consumed * $price * $praudit->product->size->units_of_measure;

                    $avg_daily_sales = empty($praudit->audit->total_days) ? 0 :  (float)$praudit_consumed_at_retail / (float)$praudit->audit->total_days;

                    $code = (int) $praudit->product->code;

                    $row->update([
                        'code' => $code,
                        'cost' => $cost,
                        'price' => $price,
                        'opening_stock' => $praudit->opening_stock,
                        'purchases' => $praudit->deliveries,
                        'credits' => $praudit->credits,
                        //'closing_stock' => $praudit_closing_stock_at_cost,
                        'closing_stock' => $praudit->closing_stock,
                        'consumed' => $consumed,
                        'transfers' => $praudit->transfers,
                        'days_stock' => $days_stock,
                        'gross_profit' => $gp,
                        'stock_at_cost' => $stock_at_cost,
                        'ballance' => $praudit_consumed_at_retail,
                        'breakage' => $consumed_at_cost,
                        'avg_daily_sales' => $avg_daily_sales,
                    ]);

                    $items->push($row);
                }

                $subtotal = new ReportRow($fixed, $accumulables, ['group' => 'pt' . $product_type->id, 'group_subtotal' => true]);
                $subtotal->set('name', 'Subtotal for ' . $product_type->name);
                $items->push($subtotal);
            }


        }

        $report = $items->aggregated($accumulables);

        return $report;
    }


    /**
     *
     * Returns columns aggregated by Product Type for a specific
     * audit or set of audits
     * area or set of areas
     *
     * @param array $options
     * @return object
     */
    public function aggregateProductType($options = [])
    {


        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $fixed = ['name'];
        $accumulables = [
            'last_closing_stock',
            'opening_stock',
            'closing_stock',
            'transfers',
            'purchases',
            'credits',
            'consumed_at_cost',
            'consumed_retail',
            'gross_profit',
            'sales_ratio',
            'days_stock'];

        $items = new ReportCollection;

        $product_types = ProductType::with('products')->orderBy('name')->get();
        foreach($product_types as $product_type)
        {
            /**
             * Getting the product ids that belong to this product type
             */
            $products_ids_product_type = $product_type->hasProducts() ? $product_type->products->pluck('id')->all() : [];

            /**
             * Now I get all the praudits that belong to this product type
             */

            $praudits_builder = Praudit::with('product', 'audit');
            $this->addToBuilder($praudits_builder, 'product_id', $products_ids_product_type);
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area_id);
            $praudits = $praudits_builder->get();

            /*echo $audit_id.PHP_EOL;
            echo $branch_id.PHP_EOL;
            echo $area_id.PHP_EOL;
            echo join(",", $products_ids_product_type).PHP_EOL;*/
            //echo 'area_id:' . $area_id . ':' . count($praudits).PHP_EOL;

            //echo $product_type->id.'.'.$praudits->count().'.'.join(',', $products_ids_product_type).'.'.$praudits_builder->toSql().'<br>';

            if(count($praudits))
            {
                $row = new ReportRow($fixed, $accumulables);
                $row->set('name', $product_type->name);

                foreach($praudits as $praudit)
                {

                    $last_closing_stock = Stock::getLastClosingStock([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                        'branch_id' => $branch_id,
                    ]);
                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);
                    $price = Stock::getSellPrice([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                    ]);
                    $praudit_consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);


                    /*$praudit_consumed_at_retail = ConsumedAtRetail::calculate([
                        'consumed' => $consumed,
                        'price' => $price
                    ]);*/
                    $praudit_consumed_at_retail = Stock::getTillCheckConsumption([
                        'audit_id' => $audit_id,
                        'area_id' => $praudit->area_id,
                        'product_id' => $praudit->product_id,
                        'sell_price' => $price,
                    ]);
                    $praudit_gross_profit = GrossProfit::calculate([
                        'type' => \App\Managers\Calculators\GrossProfit::ACTUAL,
                        'consumed' => $consumed,
                        'consumed_at_retail' => $praudit_consumed_at_retail,
                        'vat' => $this->vat,
                        'units' => $praudit->product->size->units_of_measure,
                        'cost' => $cost]);

                    $days_stock = DaysInStock::calculate([
                        'consumed_at_cost' => $praudit_consumed_at_cost,
                        'days_in_period' => $praudit->audit->total_days,
                        'closing_stock' => $praudit->closing_stock,
                    ]);

                    $praudit_purchased_at_cost = PurchasesAtCost::calculate([
                        'purchases' => $praudit->deliveries,
                        'cost' => $cost
                    ]);

                    $praudit_transfers_at_cost = TransfersAtCost::calculate([
                        'transfers' => abs($praudit->transfers) * .5,
                        'cost' => $cost
                    ]);
                    // check reports transfers issue
                    // echo $product_type->name . " aqui:" . $praudit_transfers_at_cost . "\n";
                    // echo($praudit->product_id . ' ' . $praudit->transfers) . "<br>";
                    $praudit_credits_at_cost = CreditsAtCost::calculate([
                        'credits' => $praudit->credits,
                        'cost' => $cost
                    ]);

                    $praudit_closing_stock_at_cost = ClosingStockAtCost::calculate([
                       'closing_stock' => $praudit->closing_stock,
                       'cost' => $cost,
                    ]);

                    $praudit_opening_stock = $praudit->opening_stock * $cost;

                    $row->update([
                        'last_closing_stock' => $last_closing_stock,
                        'opening_stock' => $praudit_opening_stock, //$praudit->opening_stock,
                        'closing_stock' => $praudit_closing_stock_at_cost,
                        'purchases' => $praudit_purchased_at_cost, //$praudit->deliveries,
                        'transfers' => $praudit_transfers_at_cost, //$praudit->transfers,
                        'consumed_at_cost' => $praudit_consumed_at_cost,
                        'consumed_retail' => $praudit_consumed_at_retail,
                        'gross_profit' => $praudit_gross_profit,
                        'days_stock' => $days_stock,
                        'credits' => $praudit_credits_at_cost, //$praudit->credits,
                    ]);
                    // echo('row2: ' . $row->find('name') . ': ' . $row->find('transfers').PHP_EOL);
                }

                $items->push($row);
            }

        }

        $total_consumed_retail = 0;
        foreach($items as $row)
        {
            $total_consumed_retail += (float)$row->find('consumed_retail');
        }
        foreach($items as $row)
        {
            $sales_ratio = SalesRatio::calculate([
                'amount' => $row->find('consumed_retail'),
                'total' => $total_consumed_retail,
            ]);
            $row->set('sales_ratio', $sales_ratio);
        }

        $report = $items->aggregated($accumulables);

        return $report;
    }




    /**
     *
     * Returns the stock on hand value for each product type
     * for a specific audit
     *
     * @param array $options
     * @return object
     */
    public function aggregateStockOnHand($options = [])
    {


        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $fixed = ['name'];
        $accumulables = [
            'stock_at_cost'
        ];

        $items = new ReportCollection;

        $product_types = ProductType::with('products')->orderBy('name')->get();
        foreach($product_types as $product_type)
        {
            /**
             * Getting the product ids that belong to this product type
             */
            $products_ids_product_type = $product_type->hasProducts() ? $product_type->products->pluck('id')->all() : [];

            /**
             * Now I get all the praudits that belong to this product type
             */
            $praudits_builder = Praudit::with('product', 'audit');
            $this->addToBuilder($praudits_builder, 'product_id', $products_ids_product_type);
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area_id);
            $praudits = $praudits_builder->get();

            //echo $product_type->id.'.'.$praudits->count().'.'.join(',', $products_ids_product_type).'.'.$praudits_builder->toSql().'<br>';

            if(count($praudits))
            {
                $row = new ReportRow($fixed, $accumulables);
                $row->set('name', $product_type->name);

                foreach($praudits as $praudit)
                {
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);
                    $stock_at_cost = StockAtCost::calculate([
                        'closing' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);

                    $row->update([
                        'stock_at_cost' => $stock_at_cost,
                    ]);
                }

                $items->push($row);
            }
            else
            {
                $row = new ReportRow($fixed, $accumulables);
                $row->set('name', $product_type->name);
                $row->update('stock_at_cost', 0);
                $items->push($row);
            }

        }
        $report = $items->raw();

        return $report;
    }


    /**
     *
     * Returns the stock on hand value total
     * for a specific audit
     *
     * @param array $options
     * @return object
     */
    public function aggregateStockOnHandTotal($options = [])
    {


        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $fixed = [];
        $accumulables = ['value'];

        $items = new ReportCollection;
        $row = new ReportRow($fixed, $accumulables);

        $product_types = ProductType::with('products')->orderBy('name')->get();
        foreach($product_types as $product_type)
        {
            /**
             * Getting the product ids that belong to this product type
             */
            $products_ids_product_type = $product_type->hasProducts() ? $product_type->products->pluck('id')->all() : [];

            /**
             * Now I get all the praudits that belong to this product type
             */
            $praudits_builder = Praudit::with('product', 'audit');
            $this->addToBuilder($praudits_builder, 'product_id', $products_ids_product_type);
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area_id);
            $praudits = $praudits_builder->get();

            if(count($praudits))
            {

                foreach($praudits as $praudit)
                {
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);
                    $stock_at_cost = StockAtCost::calculate([
                        'closing' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);

                    $row->update('value', $stock_at_cost);
                }

            }

        }
        $items->push($row);
        $report = $items->raw();

        return $report;
    }

    /**
     * Returns:
     * - Purchases
     * - Takings Ex Vat
     * - Takings Inc Vat
     * - Gross Profit
     * - Stock On Hand
     *
     * @param array $options
     */
    public function aggregateValuationCertificateFigures($options = [])
    {

        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $fixed = ['name'];
        $accumulables = ['value'];

        $items = new ReportCollection;

        /**
         * PURCHASES
         * -------------------------
         */
        $rowPurchases = new ReportRow($fixed, $accumulables);
        $rowPurchases->set('name', 'Purchases');

        $delivery_builder = Delivery::with('preliveries');
        $this->addToBuilder($delivery_builder, 'audit_id', $audit_id);
        $deliveries = $delivery_builder->get();
        foreach($deliveries as $delivery)
        {
            foreach($delivery->preliveries as $prelivery)
            {
                $amount = (float)$prelivery->quantity * (float)$prelivery->cost;
                $rowPurchases->update('value', $amount);
            }
        }
        $items->push($rowPurchases);

        /**
         * TAKINGS EX/INC VAT
         * -------------------------
         */
        $rowExVat = new ReportRow($fixed, $accumulables);
        $rowExVat->set('name', 'Takings Ex Vat');

        $rowIncVat = new ReportRow($fixed, $accumulables);
        $rowIncVat->set('name', 'Takings Inc Vat');

        $receipts_builder = Receipt::with('receincepts');
        $this->addToBuilder($receipts_builder, 'audit_id', $audit_id);
        $receipts = $receipts_builder->get();
        foreach($receipts as $receipt)
        {
            foreach($receipt->receincepts as $receincept)
            {
                $amount = (float)$receincept->takings_1 + (float)$receincept->takings_2 + (float)$receincept->takings_3;
                $rowIncVat->update('value', $amount);
                $rowExVat->update('value', $amount / $this->vat);
            }
        }
        $items->push($rowExVat);
        $items->push($rowIncVat);


        /**
         * STOCK ON HAND & GP
         * -------------------------
         */
        $praudits_builder = Praudit::with('product', 'audit');
        $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
        $this->addToBuilder($praudits_builder, 'area_id', $area_id);
        $praudits = $praudits_builder->get();

        $rowStockOnHand = new ReportRow($fixed, $accumulables);
        $rowStockOnHand->set('name', 'Stock on Hand');

        $rowGrossProfit = new ReportRow($fixed, $accumulables);
        $rowGrossProfit->set('name', 'Gross Profit');


        if(count($praudits))
        {
           foreach($praudits as $praudit)
           {
               $cost = Stock::getCost([
                   'product_id' => $praudit->product_id,
                   'audit_id' => $audit_id,
                   'branch_id' => $branch_id,
               ]);

               $price = Stock::getSellPrice([
                   'product_id' => $praudit->product_id,
                   'area_id' => $praudit->area_id,
               ]);

               $consumed = Consumed::calculate([
                   'opening' => $praudit->opening_stock,
                   'closing' => $praudit->closing_stock,
                   'deliveries' => $praudit->deliveries,
                   'transfers' => $praudit->transfers,
                   'credits' => $praudit->credits,
               ]);

               $gp = GrossProfit::calculate([
                   'type' => \App\Managers\Calculators\GrossProfit::REAL,
                   'consumed' => $consumed,
                   'price' => $price,
                   'vat' => $this->vat,
                   'cost' => $cost,
               ]);

               $stock_at_cost = StockAtCost::calculate([
                   'closing' => $praudit->closing_stock,
                   'cost' => $cost,
               ]);

               $rowStockOnHand->update('value', $stock_at_cost);
               $rowGrossProfit->update('value', $gp);
           }


        }
        $items->push($rowGrossProfit);
        $items->push($rowStockOnHand);


        /**
         * Other Figures
         * -------------------------
         */
        /*$rowAdjustment = new ReportRow($fixed, $accumulables);
        $rowAdjustment->set('name', 'Adjustment');
        $items->push($rowAdjustment);

        $rowAdjusted = new ReportRow($fixed, $accumulables);
        $rowAdjusted->set('name', 'Adjustment Stock Value');
        $items->push($rowAdjusted);*/

        $report = $items->raw();

        return $report;

    }


    /**
     *
     * Returns values aggregated by Area for a specific audit or set of audits
     *
     * @param array $options
     * @return object
     */
    public function aggregateAreas($options = [])
    {

        $audit_id = $this->getOptions('audit_id', $options);
        $branch_id = $this->getOptions('branch_id', $options);

        $fixed = ['name'];
        $accumulables = ['consumed_at_cost', 'income_ex_vat', 'gp', 'estimated_gp', 'actual_gp', 'surplus', 'allowances'];
        $items = new ReportCollection;

        $areas = Area::ofBranch($branch_id)
            ->sortBy(['is_cellar' => 'desc', 'name' => 'asc'])
            ->get();

        $areas->each(function($area) use ($audit_id, $branch_id, $fixed, $accumulables, &$items){

            $praudits_builder = Praudit::with('product');
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area->id);
            $praudits = $praudits_builder->get();

            if(count($praudits))
            {
                $row = new ReportRow($fixed, $accumulables);
                $row->set('name', $area->name);

                $praudits->each(function($praudit) use ($audit_id, $branch_id, &$row) {

                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);

                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);

                    $price = Stock::getSellPrice([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                    ]);

                    $consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);

                    /*$consumed_at_retail = ConsumedAtRetail::calculate([
                        'consumed' => $consumed,
                        'price' => $price
                    ]);*/

                    $consumed_at_retail = Stock::getTillCheckConsumption([
                        'audit_id' => $audit_id,
                        'area_id' => $praudit->area_id,
                        'product_id' => $praudit->product_id,
                        'sell_price' => $price,
                    ]);

                    $income_ex_vat = IncomeExVat::calculate([
                        'consumed' => $consumed,
                        'price' => $price,
                        'vat' => $this->vat,
                    ]);

                    $gp = GrossProfit::calculate([
                        'type' => \App\Managers\Calculators\GrossProfit::SIMPLE,
                        'consumed' => $consumed,
                        'price' => $price,
                        'vat' => $this->vat,
                        'cost' => $cost]);

                    $estimated_gp = GrossProfit::calculate([
                        'type' => \App\Managers\Calculators\GrossProfit::ACTUAL,
                        'consumed' => $consumed,
                        'consumed_at_retail' => $consumed_at_retail,
                        'cost' => $cost,
                        'vat' => $this->vat,
                    ]);

                    $actual_gp = GrossProfit::calculate([
                        'type' => \App\Managers\Calculators\GrossProfit::REAL,
                        'consumed' => $consumed,
                        'price' => $price,
                        'vat' => $this->vat,
                        'cost' => $cost,
                    ]);

                    $row->update([
                        'consumed_at_cost' => $consumed_at_cost,
                        'income_ex_vat' => $income_ex_vat,
                        'gp' => $gp,
                        'estimated_gp' => $estimated_gp,
                        'actual_gp' => $actual_gp,
                    ]);
                });

                $allowance = Stock::getAllowance([
                    'area_id' => $area->id,
                    'audit_id' => $audit_id,
                ]);
                $row->update('allowances', $allowance);

                $surplus = Stock::getSurplus([
                    'area_id' => $area->id,
                    'audit_id' => $audit_id,
                    'income_ex_vat' => $row->find('income_ex_vat'),
                    'allowance' => $allowance,
                ]);
                $row->update('surplus', $surplus);


                $items->push($row);

            }


        });

        return  $items->aggregated($accumulables);;

    }

    /**
     *
     * Returns values aggregated by product type for
     * a specific audit or set of audits
     *
     * @param array $options
     * @return object
     */
    public function aggregateCellar($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);
        $area_id = $this->getOptions('area_id', $options);

        $cellar = Area::ofBranch($branch_id)->where(['is_cellar' => 1])->first();
        $area_id = $cellar->id;

        $fixed = ['name'];
        $accumulables = [
            'last_closing_stock',
            'purchases',
            'credits',
            'opening_stock',
            'stock_at_cost',
            'consumed_at_cost',
            'estimated_transfers',
            'transfers',
            'overage',
            'shortage',
            'ballance',];

        $items = new ReportCollection;

        $product_types = ProductType::with('products')->orderBy('name')->get();
        foreach($product_types as $product_type)
        {
            /**
             * Getting the product ids that belong to this product type
             */
            $products_ids_product_type = $product_type->hasProducts() ? $product_type->products->pluck('id')->all() : [];

            /**
             * Now I get all the praudits that belong to this product type
             */
            $praudits_builder = Praudit::with('product');
            $this->addToBuilder($praudits_builder, 'product_id', $products_ids_product_type);
            $this->addToBuilder($praudits_builder, 'audit_id', $audit_id);
            $this->addToBuilder($praudits_builder, 'area_id', $area_id);
            $praudits = $praudits_builder->get();

            if(count($praudits))
            {
                $row = new ReportRow($fixed, $accumulables);
                $row->set('name', $product_type->name);

                foreach($praudits as $praudit)
                {
                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit_id,
                        'branch_id' => $branch_id,
                    ]);
                    $consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);
                    $stock_at_cost = StockAtCost::calculate([
                        'closing' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);
                    $last_closing_stock = Stock::getLastClosingStock([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                        'branch_id' => $branch_id,
                    ]);
                    $overage = Overage::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                        'deliveries' => $praudit->deliveries,
                    ]);
                    $shortage = Shortage::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                        'deliveries' => $praudit->deliveries,
                    ]);
                    $ballance = Balance::calculate([
                        'overage' => $overage,
                        'shortage' => $shortage,
                    ]);


                    $praudit_opening_stock = (float)$praudit->opening_stock * (float)$cost;

                    $row->update([
                        'purchases' => $praudit->deliveries,
                        'opening_stock' => $praudit_opening_stock, //$praudit->opening_stock,
                        'consumed_at_cost' => $consumed_at_cost,
                        'transfers' => $praudit->transfers,
                        'stock_at_cost' => $stock_at_cost,
                        'last_closing_stock' => $last_closing_stock,
                        'credits' => $praudit->credits,
                        'overage' => $overage,
                        'shortage' => $shortage,
                        'ballance' => $ballance,
                    ]);
                }
                $items->push($row);
            }

        }
        return $items->aggregated($accumulables);
    }

    /**
     *
     * Returns delivery notes aggregated for a specific audit
     *
     * @param array $options
     * @return object
     */
    public function aggregateDeliveries($options = []){

        $branch_id = $this->getOptions('branch_id', $options);
        $audit_id = $this->getOptions('audit_id', $options);

        $fixed = ['name', 'number', 'order_number', 'date', 'ref_number'];
        $accumulable = ['value'];
        $items = new ReportCollection;

        $deliveries_builder = Delivery::with('supplier', 'preliveries')
            ->orderBy('id', 'desc');
        $this->addToBuilder($deliveries_builder, 'branch_id', $branch_id);
        $this->addToBuilder($deliveries_builder, 'audit_id', $audit_id);
        $deliveries = $deliveries_builder->get();

        $deliveries->each(function($delivery) use ($branch_id, $audit_id, $fixed, $accumulable, &$items){

            $row = new ReportRow($fixed, $accumulable);
            $row->set('name', $delivery->supplier->name);
            $row->set('number', $delivery->code);
            $row->set('order_number', $delivery->order);
            $row->set('date', $delivery->date_formatted);
            $row->set('ref_number', $delivery->invoice);

            if($delivery->hasPreliveries())
            {
                $delivery->total = 0;
                $delivery->preliveries->each(function($prelivery) use ($branch_id, $audit_id, &$delivery, &$items) {
                    $prelivery->subtotal = $prelivery->quantity * $prelivery->cost;
                    $delivery->total += $prelivery->subtotal;
                });
                $row->update('value', $delivery->total);
            }

            $items->push($row);

        });

        return $items->aggregated($accumulable);

    }

    /**
     *
     * Returns closing stock aggregated by audit_date for plots
     *
     * @param array $options
     */
    public function aggregateClosingStock($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $date_start = $this->getOptions('date_start', $options);
        $date_end = $this->getOptions('date_end', $options);

        $items = new ReportCollection;

        $audits = Audit::with('praudits', 'praudits.product')
            ->where(['branch_id' => $branch_id])
            ->whereBetween('audit_date', [$date_start, $date_end])
            ->orderBy('audit_date')
            ->get();

        foreach($audits as $audit)
        {
            $row = new ReportRow(['label'], ['data']);
            $row->set('label', $audit->audit_date_formatted_short);
            if(count($audit->praudits))
            {
                foreach($audit->praudits as $praudit)
                {
                    $row->update(['data' => $praudit->closing_stock]);
                }
            }
            $items->push($row);

        }

        return $items->raw();

    }

    /**
     *
     * Returns opening stock agregated by audit_date for plots
     *
     * @param array $options
     */
    public function aggregateOpeningStock($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $date_start = $this->getOptions('date_start', $options);
        $date_end = $this->getOptions('date_end', $options);

        $items = new ReportCollection;

        $audits = Audit::with('praudits', 'praudits.product')
            ->where(['branch_id' => $branch_id])
            ->whereBetween('audit_date', [$date_start, $date_end])
            ->orderBy('audit_date')
            ->get();

        foreach($audits as $audit)
        {
            $row = new ReportRow(['label'], ['data']);
            $row->set('label', $audit->audit_date_formatted_short);
            if(count($audit->praudits))
            {
                foreach($audit->praudits as $praudit)
                {
                    $row->update(['data' => $praudit->opening_stock]);
                }
            }
            $items->push($row);

        }

        return $items->raw();

    }

    /**
     *
     * Returns consumed at cost aggregated by audit_date for plots
     *
     * @param array $options
     */
    public function aggregateConsumedAtCost($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $date_start = $this->getOptions('date_start', $options);
        $date_end = $this->getOptions('date_end', $options);

        $items = new ReportCollection;

        $audits = Audit::with('praudits', 'praudits.product')
            ->where(['branch_id' => $branch_id])
            ->whereBetween('audit_date', [$date_start, $date_end])
            ->orderBy('audit_date')
            ->get();

        foreach($audits as $audit)
        {
            $row = new ReportRow(['label'], ['data']);
            $row->set('label', $audit->audit_date_formatted_short);
            if(count($audit->praudits))
            {
                foreach($audit->praudits as $praudit)
                {
                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit->id,
                        'branch_id' => $branch_id,
                    ]);
                    $praudit_consumed_at_cost = ConsumedAtCost::calculate([
                        'consumed' => $consumed,
                        'cost' => $cost
                    ]);
                    $row->update(['data' => $praudit_consumed_at_cost]);
                }
            }
            $items->push($row);

        }

        return $items->raw();

    }

    /**
     *
     * Returns stock at cost aggregated by audit_date for plots
     *
     * @param array $options
     */
    public function aggregateStockAtCost($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $date_start = $this->getOptions('date_start', $options);
        $date_end = $this->getOptions('date_end', $options);

        $items = new ReportCollection;

        $audits = Audit::with('praudits', 'praudits.product')
            ->where(['branch_id' => $branch_id])
            ->whereBetween('audit_date', [$date_start, $date_end])
            ->orderBy('audit_date')
            ->get();

        foreach($audits as $audit)
        {
            $row = new ReportRow(['label'], ['data']);
            $row->set('label', $audit->audit_date_formatted_short);
            if(count($audit->praudits))
            {
                foreach($audit->praudits as $praudit)
                {
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit->id,
                        'branch_id' => $branch_id,
                    ]);
                    $stock_at_cost = StockAtCost::calculate([
                        'closing' => $praudit->closing_stock,
                        'cost' => $cost,
                    ]);
                    $row->update(['data' => $stock_at_cost]);
                }
            }
            $items->push($row);

        }

        return $items->raw();

    }

    /**
     *
     * Returns gross profit aggregated by audit_date for plots
     *
     * @param array $options
     */
    public function aggregateGrossProfit($options = [])
    {
        $branch_id = $this->getOptions('branch_id', $options);
        $date_start = $this->getOptions('date_start', $options);
        $date_end = $this->getOptions('date_end', $options);

        $items = new ReportCollection;

        $audits = Audit::with('praudits', 'praudits.product')
            ->where(['branch_id' => $branch_id])
            ->whereBetween('audit_date', [$date_start, $date_end])
            ->orderBy('audit_date')
            ->get();

        foreach($audits as $audit)
        {
            $row = new ReportRow(['label'], ['data']);
            $row->set('label', $audit->audit_date_formatted_short);
            if(count($audit->praudits))
            {
                foreach($audit->praudits as $praudit)
                {
                    $consumed = Consumed::calculate([
                        'opening' => $praudit->opening_stock,
                        'closing' => $praudit->closing_stock,
                        'deliveries' => $praudit->deliveries,
                        'transfers' => $praudit->transfers,
                        'credits' => $praudit->credits,
                    ]);
                    $cost = Stock::getCost([
                        'product_id' => $praudit->product_id,
                        'audit_id' => $audit->id,
                        'branch_id' => $branch_id,
                    ]);
                    $price = Stock::getSellPrice([
                        'product_id' => $praudit->product_id,
                        'area_id' => $praudit->area_id,
                    ]);
                    $praudit_gross_profit = GrossProfit::calculate([
                        'type' => \App\Managers\Calculators\GrossProfit::REAL,
                        'consumed' => $consumed,
                        'price' => $price,
                        'vat' => $this->vat,
                        'units' => $praudit->product->size->units_of_measure,
                        'cost' => $cost]);
                    $row->update(['data' => $praudit_gross_profit]);
                }
            }
            $items->push($row);

        }

        return $items->raw();

    }
}
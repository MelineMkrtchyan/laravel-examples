<?php

namespace App\Http\Controllers\API;

use App\Entities\ClientCode;
use App\Entities\Praudit;
use App\Entities\Product;
use App\Entities\Size;
use App\Entities\SizeClient;
use App\Http\Controllers\Controller;
use Facades\App\Managers\Calculators\Consumed;
use Facades\App\Managers\Calculators\GrossProfit;
use Facades\App\Managers\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{

    protected $client_id;
    protected $branch_id;
    protected $audit_id;
    protected $area_id;

    public function __construct()
    {
        $this->client_id = request()->route('client_id') ? request()->route('client_id') : request()->input('client_id');
        $this->branch_id = request()->route('branch_id') ? request()->route('branch_id') : request()->input('branch_id');
        $this->audit_id = request()->route('audit_id') ? request()->route('audit_id') : request()->input('audit_id');
        $this->area_id = request()->route('area_id') ? request()->route('area_id') : request()->input('area_id');
    }

    public function getSizes()
    {
        return Size::all();
    }

    public function getProducts()
    {

        $products = Product::with('type', 'subtype')
            ->orderBy('product_type_id')
            ->orderBy('product_subtype_id')
            ->orderBy('description');


        if ($this->area_id && $this->audit_id) {
            $products_already_in_area = Praudit::where(['audit_id' => $this->audit_id, 'area_id' => $this->area_id])
                ->select('product_id')
                ->get()
                ->pluck('product_id');
            $products = $products->whereNotIn('id', $products_already_in_area);
        }

        $products = $products->get();

        $result = [];
        foreach ($products as $product) {

            $result[] = [
                'id' => $product->id,
                'code' => $product->code,
                'name' => $product->description,
                'size' => $product->size->short_description,
                'type_description' => $product->type->name . '/' . $product->subtype->name,
                'type' => $product->product_type_id,
                'subtype' => $product->product_subtype_id,
                'cost' => '12.00',
                'suppliers' => $product->suppliers,
            ];

        }
        return response()->json($result);
    }

    public function getStock($audit_id, $area_id)
    {
        $mode = request()->input('mode');

        $items = Praudit::with('product', 'product.type', 'product.subtype')
            ->thisAudit()
            ->thisArea()
            ->get();

        $result = [];
        foreach ($items as $item) {

            if ($mode == "cellar") {
                $data = [
                    'id' => $item->product->id,
                    'praudit_id' => $item->id,
                    'code' => $item->product->code,
                    'client_code' => $item->product->hasClientCode() ? $item->product->client_code->first()->code_formatted : 'N/A',
                    'description' => $item->product->description,
                    'short_description' => $item->product->size->short_description,
                    'unit_description' => $item->product->size->unit_description,
                    'count' => $item->product->size->count,
                    'units_of_measure' => $item->product->size->units_of_measure,
                    'type' => $item->product->type->id,
                    'subtype' => $item->product->subtype->id,
                    'cost' => $this->getProductCost($item->product_id),
                    'opening_stock' => $item->opening_stock,
                    'closing_stock' => $item->closing_stock,
                    'deliveries' => $item->deliveries,
                    'transfers' => $item->transfers,
                    'credits' => $item->credits,
                    'vat' => '1.2',
                ];
                $data['consumed'] = $this->getConsumed($data);
                $result[] = $data;
            } else {
                $data = [
                    'id' => $item->product->id,
                    'praudit_id' => $item->id,
                    'code' => $item->product->code,
                    'client_code' => $item->product->hasClientCode() ? $item->product->client_code->first()->code_formatted : 'N/A',
                    'description' => $item->product->description,
                    'short_description' => $item->product->size->short_description,
                    'unit_description' => $item->product->size->unit_description,
                    'count' => $item->product->size->count,
                    'units_of_measure' => $item->product->size->units_of_measure,
                    'type' => $item->product->type->id,
                    'subtype' => $item->product->subtype->id,
                    'cost' => $this->getProductCost($item->product_id),
                    'opening_stock' => $item->opening_stock,
                    'closing_stock' => $item->closing_stock,
                    'selling_price' => $this->getSellPrice($item->product_id),
                    'transfers' => $item->transfers,
                    'credits' => $item->credits,
                    'vat' => '1.2',
                ];


                if ($data['code'] == '0047') {
                }

                $data['consumed'] = $this->getConsumed($data);
                $data['gp'] = $this->getGPNew($data);
                // dd($data['gp']);
                $result[] = $data;
            }

        }


//        exit('done');
        return response()->json($result);

    }

    private function getGPNew($data)
    {

        $gp = GrossProfit::newCalculate([
            'cost' => $data['cost'],
            'price' => $data['selling_price'],
            'units' => $data['units_of_measure'],
            'vat' => $data['vat'],
        ]);

        return $this->format($gp);
    }


    public function updateStock(Request $request)
    {
        $product_id = $request->input('id');
        $praudit_id = $request->input('praudit_id');
        $selling_price = $request->input('selling_price');
        $cost = $request->input('cost');
        $client_code = $request->input('client_code');
        $opening_stock = $request->input('opening_stock');
        $closing_stock = $request->input('closing_stock');
        $unit_description = $request->input('unit_description');
        $units_of_measure = $request->input('units_of_measure');
        $short_description = $request->input('short_description');
        $long_description = $request->input('long_description');
        $count = $request->input('count');
        $unit_size = $request->input('unit_size');
        $barrellage = $request->input('barrellage');

        if ($client_code != "" && $client_code != 'N/A') {
            $client_code = ClientCode::firstOrCreate([
                'client_id' => $this->client_id,
                'product_id' => $product_id,
            ])
                ->update(['code' => $client_code]);
        }

        if ($unit_description != '' && $units_of_measure != '' && $short_description != '') {
            $client_size = SizeClient::firstOrCreate([
                'client_id' => $this->client_id,
                'product_id' => $product_id,
            ])->update([
                'short_description' => $short_description,
                'units_of_measure' => $units_of_measure,
                'unit_description' => $unit_description,
                'long_description' => $long_description,
                'count' => $count,
                'unit_size' => $unit_size,
                'barrellage' => $barrellage,
            ]);
        }

        if ($selling_price) {
            $price = Stock::pushSellPrice([
                'branch_id' => $this->branch_id,
                'audit_id' => $this->audit_id,
                'area_id' => $this->area_id,
                'product_id' => $product_id,
                'price' => $selling_price,
            ]);
        }

//        dd($cost);
        if ($cost) {
            $product_cost = Stock::pushCost([
                'audit_id' => $this->audit_id,
                'branch_id' => $this->branch_id,
                'product_id' => $product_id,
                'cost' => $cost,
            ]);
        }

        $stock = [];
        $prAudit = Praudit::find($praudit_id);
        if ($opening_stock) {
            $newOpStock = $stock['opening_stock'] = $opening_stock;
            $oldOpStock = $prAudit->opening_stock;
            $differenceOpStock = $oldOpStock - $newOpStock;
            $stock['availability'] = $prAudit->availability - $differenceOpStock;
        }
        if ($closing_stock) $stock['closing_stock'] = $closing_stock;
        if ($opening_stock || $closing_stock) {
            $prAudit->update($stock);
        }


        return response()->json(['ok' => 1]);
    }

    private function getConsumed($data)
    {
        $consumed = Consumed::calculate([
            'opening' => $data['opening_stock'],
            'closing' => $data['closing_stock'],
            'transfers' => $data['transfers'],
            'deliveries' => array_key_exists('deliveries', $data) ? $data['deliveries'] : 0,
            'credits' => array_key_exists('credits', $data) ? $data['credits'] : 0,
        ]);

        return $this->format($consumed);
    }

    private function getGP($data)
    {

        $gp = GrossProfit::calculate([
            'type' => \App\Managers\Calculators\GrossProfit::REAL,
            'consumed' => $data['consumed'],
            'price' => $data['selling_price'],
            'cost' => $data['cost'] / $data['units_of_measure'],
            'vat' => $data['vat'],
        ]);

        return $this->format($gp);

    }

    private function getProductCost($product_id)
    {
        $cost = Stock::getCost([
            'product_id' => $product_id,
            'audit_id' => $this->audit_id,
            'branch_id' => $this->branch_id,
        ]);
        return $cost;
    }

    private function getSellPrice($product_id)
    {
        $price = Stock::getSellPrice([
            'product_id' => $product_id,
            'area_id' => $this->area_id,
        ]);

        return ffn($price);
    }

    private function format($number)
    {
        return number_format((float)$number, 2, '.', '');
    }


}

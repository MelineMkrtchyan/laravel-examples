<?php

namespace App\Managers\Calculators;

use App\Managers\Manager;

class GrossProfit extends Manager implements CalculatorContract
{
    const SIMPLE = 'simple';
    const REAL = 'real';
    const ACTUAL = 'actual';
    const ESTIMATED = 'estimated';

    public function newCalculate(array $options)
    {
        $cost = $this->getOptions('cost', $options, 0);
        $price = $this->getOptions('price', $options, 0);
        $units = $this->getOptions('units', $options, 0);
        $vat = $this->getOptions('vat', $options, 0);

        $sales = (float)$price * (float)$units / (float)$vat;
        $profit = (float)$sales - (float)$cost;

        if(!$sales){
            return 0;
        }

        return (float)$profit * 100 / (float)$sales;

    }

    public function calculate(array $options)
    {
        $type = $this->getOptions('type', $options);
        switch ($type) {
            case GrossProfit::SIMPLE:
                return $this->simple($options);
                brrak;
            case GrossProfit::REAL:
                return $this->real($options);
                brrak;
            case GrossProfit::ESTIMATED:
                return $this->estimated($options);
                brrak;
            case GrossProfit::ACTUAL:
                return $this->actual($options);
                brrak;
        }
    }

    private function simple($options)
    {
        $consumed = $this->getOptions('consumed', $options, 0);
        $sell_price = $this->getOptions('price', $options, 0);
        $cost_price = $this->getOptions('cost', $options, 0);
        $vat = $this->getOptions('vat', $options, 0);

        $income_ex_vat = ((float)$consumed * (float)$sell_price) / (float)$vat;
        $consumed_at_cost = (float)$consumed * (float)$cost_price;
        $gp = (float)$income_ex_vat - (float)$consumed_at_cost;

        return $gp < 0 ? 0 : $gp;
    }

    private function real($options)
    {
        $consumed = $this->getOptions('consumed', $options, 0);
        $sell_price = $this->getOptions('price', $options, 0);
        $cost_price = $this->getOptions('cost', $options, 0);
        $vat = $this->getOptions('vat', $options, 0);

        $income_ex_vat = ((float)$consumed * (float)$sell_price) / (float)$vat;

        $consumed_at_cost = (float)$consumed * (float)$cost_price;
        $gp = (float)$income_ex_vat - (float)$consumed_at_cost;
        $gpp = $income_ex_vat == 0 ? 0 : ((float)$gp / (float)$income_ex_vat) * 100;

        /*dd([
           'consumed' => $consumed,
           'sell_price' => $sell_price,
           'cost_price' => $cost_price,
           'var' => $vat,
           'income_ex_vat' => $income_ex_vat,
           'consumed_at_cost' => $consumed_at_cost,
           'gp' => $gp,
        ]);*/

        return $gpp;
    }

    private function estimated($options)
    {
        $consumed_at_cost = $this->getOptions('consumed_at_cost', $options);
        $consumed_at_retail = $this->getOptions('consumed_at_retail', $options);
        $vat = $this->getOptions('vat', $options);

        $estimated_gp = 0;
        $consumed_at_cost_ex_vat = $consumed_at_cost / $vat;
        $consumed_at_retail_ex_vat = $consumed_at_retail / $vat;

        if ($consumed_at_cost_ex_vat > 0) {
            $estimated_gp = (((float)$consumed_at_retail_ex_vat - (float)$consumed_at_cost) / $consumed_at_cost_ex_vat) * 100;
        }
        return $estimated_gp;
    }

    private function actual($options)
    {

        $consumed_at_retail = $this->getOptions('consumed_at_retail', $options, 0);
        $consumed = $this->getOptions('consumed', $options, 0);
        $cost_price = $this->getOptions('cost', $options, 0);
        $vat = $this->getOptions('vat', $options, 0);

        $consumed_at_retail_ex_vat = ((float)$consumed_at_retail) / (float)$vat;
        $consumed_at_cost = (float)$consumed * (float)$cost_price;
        $gp = (float)$consumed_at_retail_ex_vat - (float)$consumed_at_cost;
        $gpp = $consumed_at_retail_ex_vat == 0 ? 0 : ((float)$gp / (float)$consumed_at_retail_ex_vat) * 100;

        return $gpp;
    }
}